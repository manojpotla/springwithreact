import React from 'react';
import { Link } from 'react-router-dom';

const CustomButton = (props) => {
    return (
        <div>
            <Link to="/NewProject" className="btn btn-lg btn-info">
                {props.label}
            </Link>
        </div>
    );
}

export default CustomButton;

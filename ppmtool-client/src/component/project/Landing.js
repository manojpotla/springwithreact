import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class LandingPage extends Component {
    render() {
        return (
            <div className="landing" >
                <div className="light-overlay landing-inner text-dark">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <h1 className="display-3 mb-4">Personal Kanban Tool</h1>
                                <p className="lead">
                                    Create your account to join active projects or start you own
                            </p>
                                <hr />
                                <Link to="/SignUp" className="btn btn-lg btn-primary mr-2">
                                    Sign Up
                                </Link>
                                <Link to="/Login" className="btn btn-lg btn-primary mr-2">
                                    Login
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    };
}

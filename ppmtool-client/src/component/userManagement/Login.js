import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { loginUser } from '../../actions/loginAction';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            errors: {},
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
        if (nextProps.security.validToken) {
            this.props.history.push("/dashboard");
        }
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit = (event) => {

        event.preventDefault();
        const loginP = {
            username: this.state.username,
            password: this.state.password,
        }
        this.props.loginUser(loginP, this.props.history);

    }
    render() {
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Log In</h1>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input type="text" className="form-control form-control-lg" placeholder="User Name" name="username"
                                        value={this.state.username} onChange={this.onChange} />
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control form-control-lg" placeholder="Password" name="password"
                                        value={this.state.password} onChange={this.onChange} />
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

const mapStateToProps = (state) => ({
    security: state.securityReducer,
    errors: state.errorReducer.errors,
});

const mapDispatchToProps = {
    loginUser,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { createNewUser } from '../../actions/securityActions';

export class Register extends Component {
    constructor(props) {
        super();
        this.state = {
            fullname: '',
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            errors: {},
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = (e) => {
        const { createNewUser } = this.props;
        e.preventDefault();
        const newUser = {
            username: this.state.username,
            fullname: this.state.fullname,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
        }
        createNewUser(newUser, this.props.history);
    }

    render() {
        const { errors } = this.state;
        return (
            <div className="register">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Sign Up</h1>
                            <p className="lead text-center">Create your Account</p>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", { "is-invalid": errors.fullname })} placeholder="Full Name" name="fullname"
                                        value={this.state.fullName} onChange={this.onChange} required />
                                    {errors.fullname && (<div className="invalid-feedback">{errors.fullname}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", { "is-invalid": errors.username })} placeholder="User Name" name="username"
                                        value={this.state.userName} onChange={this.onChange} required />
                                    {errors.username && (<div className="invalid-feedback">{errors.username}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="email" className={classnames("form-control form-control-lg", { "is-invalid": errors.email })} placeholder="Email Address" name="email"
                                        value={this.state.email} onChange={this.onChange} />
                                    {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="password" className={classnames("form-control form-control-lg", { "is-invalid": errors.password })} placeholder="Password" name="password"
                                        value={this.state.password} onChange={this.onChange} />
                                    {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
                                </div>
                                <div className="form-group">
                                    <input type="password" className={classnames("form-control form-control-lg", { "is-valid": errors.confirmPassword })} placeholder="Confirm Password"
                                        name="confirmPassword" value={this.state.confirmPassword} onChange={this.onChange} />
                                    {errors.confirmPassword && (<div className="invalid-feedback">{errors.confirmPassword}</div>)}
                                </div>
                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    };
}

const mapStateToProps = (state) => ({
    errors: state.errorReducer.errors,
});

const mapDispatchToProps = {
    createNewUser,
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
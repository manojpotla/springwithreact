import axios from 'axios';
import { GET_ERRORS, SET_CURRENT_USER } from './types';
import jwt_decode from 'jwt-decode';

export const loginUser = (user, history) => async dispatch => {
    try {
        const res = await axios.post("/api/signin", user);
        console.log(res.data);
        const { jwtToken } = res.data;
        localStorage.setItem("jwtToken", jwtToken);
        setJWTToken(jwtToken);
        const decodedToken = jwt_decode(jwtToken);
        dispatch({
            type: SET_CURRENT_USER,
            payload: decodedToken
        });
    } catch (err) {
        dispatch({
            type: GET_ERRORS,
            payload: (err.response) ? err.response.data : err,
        });
    }
}

const setJWTToken = (tokenString) => {
    if (tokenString) {
        axios.defaults.headers.common["Authorization"] = tokenString;
    } else {
        delete axios.defaults.headers.common["Authorization"]
    }
}
import axios from 'axios';
import { GET_ERRORS, GET_PROJECTS, GET_PROJECT, DELETE_PROJECT } from './types';

const createProject = (project, history) => async dispatch => {
    try {
        const res = await axios.post("/api/project", project);
        history.push("/");
    } catch (err) {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        });
    }
};

export { createProject };

const getAllProjects = () => async dispatch => {
    const res = await axios.get("/api/project");
    dispatch({
        type: GET_PROJECTS,
        payload: res.data
    });
}

export { getAllProjects };

const getProject = (identifierName, history) => async dispatch => {
    const res = await axios.get(`/api/project/${identifierName}`);
    dispatch({
        type: GET_PROJECT,
        payload: res.data
    });
}

export { getProject };

const editProject = (updatedProject, history) => async dispatch => {
    try {
        const res = await axios.put(`/api/project`, updatedProject);
        history.push("/");
        dispatch({ type: GET_ERRORS, payload: {} })
    } catch (err) {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        });
    }
}

export { editProject };

const deleteProject = (projectIdentifier, history) => async dispatch => {
    const res = await axios.delete(`/api/project/${projectIdentifier}`);
    dispatch({
        type: DELETE_PROJECT,
        payload: projectIdentifier
    });

}

export { deleteProject };
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import Header from './component/Header';
import NewProject from './component/project/NewProject';
import Landing from './component/project/Landing';
import UpdateProject from './component/project/UpdateProject';
import Register from './component/userManagement/Register';
import Login from './component/userManagement/Login';
import { Provider } from 'react-redux';
import store from './store/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Route component={Header} />

            <Route exact path="/NewProject" component={NewProject} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/UpdateProject/:projectIdentifier" component={UpdateProject} />

            <Route exact path="/" component={Landing} />
            <Route exact path="/SignUp" component={Register} />
            <Route exact path="/Login" component={Login} />

          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;

import React, { Component } from 'react';
import { ProjectItem } from './component/project/ProjectItem';
import CustomButton from './component/project/CustomButton';
import { connect } from 'react-redux';
import { getAllProjects, getProject, deleteProject } from './actions/projectActions';

class Dashboard extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        this.props.getAllProjects();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps){

        }
    }
    render() {
        let buttonText = 'Create a Project';
        const { projects, deleteProject } = this.props;
        return (
            <div>
                <div className="projects">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <h1 className="display-4 text-center">Projects</h1>
                                <br />
                                <CustomButton label={buttonText} />
                                <br />
                                <hr />
                                {projects.map(project => (
                                    <ProjectItem key={project.id} project={project} deleteProject={deleteProject} />
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export { Dashboard };

const mapStateToProps = (state) => ({
    projects: state.projectReducer.projects
});

const mapDispatchToProps = {
    getAllProjects,
    getProject,
    deleteProject
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)


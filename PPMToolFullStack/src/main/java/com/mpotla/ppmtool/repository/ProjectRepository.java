package com.mpotla.ppmtool.repository;

import com.mpotla.ppmtool.domain.Project;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    @Override
    Iterable<Project> findAllById(Iterable<Long> iterable);

    Project findByProjectIdentifier(String projectIdentifier);

    Integer deleteByProjectIdentifier(String name);

    @Modifying
    @Query("update Project p set p.projectName = ?1, p.description = ?2, p.end_date = ?3 where p.projectIdentifier = ?4 ")
    int update(String projectName, String description, Date endDate, String projectIdentifier);
}


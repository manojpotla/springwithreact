package com.mpotla.ppmtool.exceptions;

public class UserNotFoundException extends RuntimeException{

    private String message;

    public UserNotFoundException(String message){
        this.message = message;
    }
}

package com.mpotla.ppmtool.exceptions;

public class UsernameExistsException extends RuntimeException{

    private String message;

    public UsernameExistsException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.mpotla.ppmtool.exceptions;

import java.util.Date;

public class ProjectPlanningServiceException extends RuntimeException {

    private String errorMessage;
    private Date exceptionTime;
    private Exception ex;
    private String fieldName;

    public ProjectPlanningServiceException(String fieldName, String errorMessage,  Date exceptionTime){
        this.errorMessage = errorMessage;
        this.exceptionTime = exceptionTime;
        this.fieldName = fieldName;
    }

    public ProjectPlanningServiceException(Exception ex, String errorMessage, Date exceptionTime) {
        this.errorMessage = errorMessage;
        this.exceptionTime =  exceptionTime;
        this.ex = ex;
    }

    public ProjectPlanningServiceException(Exception ex, String errorMessage, Date exceptionTime, String fieldName){
        this.errorMessage = errorMessage;
        this.exceptionTime =  exceptionTime;
        this.ex = ex;
        this.fieldName = fieldName;
    }
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getExceptionTime() {
        return exceptionTime;
    }

    public void setExceptionTime(Date exceptionTime) {
        this.exceptionTime = exceptionTime;
    }

    public Exception getEx() {
        return ex;
    }

    public void setEx(Exception ex) {
        this.ex = ex;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}

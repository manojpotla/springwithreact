package com.mpotla.ppmtool.exceptions;

import java.util.Date;

public class RecordNotFoundException extends RuntimeException{
    private String errorMessage;
    private Date exceptionTime;
    private String fieldName;

    public RecordNotFoundException(String fieldName, String errorMessage,  Date exceptionTime){
        this.errorMessage = errorMessage;
        this.exceptionTime = exceptionTime;
        this.fieldName = fieldName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getExceptionTime() {
        return exceptionTime;
    }

    public void setExceptionTime(Date exceptionTime) {
        this.exceptionTime = exceptionTime;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}

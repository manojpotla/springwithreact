package com.mpotla.ppmtool.exceptions;

import java.util.List;

public class ExceptionResponse {
    private String fieldName;
    private String errorMessage;

    public ExceptionResponse(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public ExceptionResponse(String fieldName, String errorMessage){
        this.fieldName = fieldName;
        this.errorMessage = errorMessage;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}

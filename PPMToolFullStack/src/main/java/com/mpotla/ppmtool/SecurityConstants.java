package com.mpotla.ppmtool;

public class SecurityConstants {

    public static final String WEB_RESOURCES = "/*.css";
    public static final String SIGN_UP_URL = "/api/register/**";
    public static final String LOG_IN_URL = "/api/signin/**";
    public static final String ROOT_URL = "/";
    public static final String H2_CONSOLE = "/h2-console/**";
    public static final String SECRET_KEY = "secretkeytoken";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 180_000;

}

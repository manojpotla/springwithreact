package com.mpotla.ppmtool.services.impl;

import com.mpotla.ppmtool.services.ApiValidationService;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.HashMap;
import java.util.Map;

@Service
public class ApiValidationServiceImpl implements ApiValidationService {

    public Map<String, String> validateApiRequest(BindingResult bindingErrors){

        if(bindingErrors.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();
            bindingErrors.getFieldErrors().forEach(fieldError -> errorMap.put(fieldError.getField(), fieldError.getDefaultMessage()));
            return errorMap;
        }
        return null;
    }
}

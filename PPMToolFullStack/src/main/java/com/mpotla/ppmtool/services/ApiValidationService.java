package com.mpotla.ppmtool.services;

import org.springframework.validation.BindingResult;

import java.util.Map;

public interface ApiValidationService {

    Map<String, String> validateApiRequest(BindingResult bindingResult);
}

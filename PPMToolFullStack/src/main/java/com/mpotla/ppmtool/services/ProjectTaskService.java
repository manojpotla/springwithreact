package com.mpotla.ppmtool.services;

import com.mpotla.ppmtool.domain.ProjectTask;

public interface ProjectTaskService {

    ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask);
}

package com.mpotla.ppmtool.services.impl;

import com.mpotla.ppmtool.domain.User;
import com.mpotla.ppmtool.exceptions.UsernameExistsException;
import com.mpotla.ppmtool.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Transactional
    public User createUser(User user){
        User existingUser = userRepository.findByUsername(user.getUsername());

        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        System.out.println(encodedPassword.length());
        if(existingUser != null){
            throw new UsernameExistsException("User Name already exists");
        }
        User createdUser = userRepository.save(user);
        return createdUser;
    }


}

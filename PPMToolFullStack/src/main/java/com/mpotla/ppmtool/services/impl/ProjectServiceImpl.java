package com.mpotla.ppmtool.services.impl;

import com.mpotla.ppmtool.domain.Backlog;
import com.mpotla.ppmtool.domain.Project;
import com.mpotla.ppmtool.exceptions.ProjectPlanningServiceException;
import com.mpotla.ppmtool.exceptions.RecordNotFoundException;
import com.mpotla.ppmtool.repository.BacklogRepository;
import com.mpotla.ppmtool.repository.ProjectRepository;
import com.mpotla.ppmtool.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;
    private BacklogRepository backlogRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, BacklogRepository backlogRepository){
        this.projectRepository = projectRepository;
        this.backlogRepository = backlogRepository;
    }

    @Transactional
    public Project saveOrUpdate(Project project){
            Project projectByIdentifier = projectRepository.findByProjectIdentifier(project.getProjectIdentifier());
            if(projectByIdentifier != null)
                throw new ProjectPlanningServiceException(new DataIntegrityViolationException("Unique Key Violation"), project.getProjectIdentifier() + " Duplicate Name" , new Date(), "projectIdentifier");

            Backlog backlog = backlogRepository.findByProjectIdentifier(project.getProjectIdentifier());

            if(backlog == null){
                backlog =  new Backlog();
                backlog.setProjectIdentifier(project.getProjectIdentifier());
                backlog.setProjectTaskSequence(0);
                project.setBacklog(backlog);
                backlog.setProject(project);
            }

            return projectRepository.save(project);
        }
    public Iterable<Project> findAllProjects(){
        return projectRepository.findAll();
    }

    @Transactional
    public String deleteProject(String name){
        Project projectByIdentifier = projectRepository.findByProjectIdentifier(name);

        if(projectByIdentifier == null)
            throw new RecordNotFoundException(name , "Project Not Found", new Date());

        projectRepository.deleteByProjectIdentifier(name);

        return "Project succesfully deleted";
    }

    public Project retrieveProject(String projectIdentifier){
        return projectRepository.findByProjectIdentifier(projectIdentifier);
    }

    @Transactional
    public Project editProject(Project project){

        projectRepository.update(project.getProjectName(),project.getDescription(),project.getEnd_date(), project.getProjectIdentifier());
        return project;
    }


}

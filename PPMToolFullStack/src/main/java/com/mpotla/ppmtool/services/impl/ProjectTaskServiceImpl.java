package com.mpotla.ppmtool.services.impl;

import com.mpotla.ppmtool.domain.Backlog;
import com.mpotla.ppmtool.domain.ProjectTask;
import com.mpotla.ppmtool.repository.BacklogRepository;
import com.mpotla.ppmtool.repository.ProjectTaskRepository;
import com.mpotla.ppmtool.services.ProjectTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectTaskServiceImpl implements ProjectTaskService {

    private BacklogRepository backlogRepository;
    private ProjectTaskRepository projectTaskRepository;

    @Autowired
    public ProjectTaskServiceImpl(BacklogRepository backlogRepository, ProjectTaskRepository projectTaskRepository){
        this.backlogRepository = backlogRepository;
        this.projectTaskRepository = projectTaskRepository;
    }

    @Override
    public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask) {

        Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);

        if(backlog !=null){
            int backlogSequence = backlog.getProjectTaskSequence();
            backlogSequence++;
            backlog.setProjectTaskSequence(backlogSequence);
            projectTask.setBacklog(backlog);
            projectTask.setProjectSequence(projectIdentifier + "-" + backlogSequence);
        }

        if(projectTask.getPriority() != null){
            projectTask.setPriority(3);
        }
        return projectTaskRepository.save(projectTask);
    }
}

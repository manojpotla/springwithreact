package com.mpotla.ppmtool.services;

import com.mpotla.ppmtool.domain.Project;

public interface ProjectService{

  Project saveOrUpdate(Project project);

  Iterable<Project> findAllProjects();

  String deleteProject(String name);

  Project retrieveProject(String projectIdentifier);

  Project editProject(Project project);
}

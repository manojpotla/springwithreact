package com.mpotla.ppmtool.controllers;

import com.mpotla.ppmtool.domain.ProjectTask;
import com.mpotla.ppmtool.services.ApiValidationService;
import com.mpotla.ppmtool.services.ProjectTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/backlog")
@CrossOrigin
public class BacklogController {

    private ProjectTaskService projectTaskServiceImpl;
    private ApiValidationService apiValidationServiceImpl;

    @Autowired
    public BacklogController(ProjectTaskService projectTaskServiceImpl, ApiValidationService apiValidationServiceImpl){
        this.projectTaskServiceImpl = projectTaskServiceImpl;
        this.apiValidationServiceImpl = apiValidationServiceImpl;
    }

    @PostMapping("/{projectIdentifier}")
    public ResponseEntity<?> addToBacklog(@RequestBody ProjectTask projectTask, @PathVariable("projectIdentifier") String projectIdentifier){

        return new ResponseEntity<>(projectTaskServiceImpl.addProjectTask(projectIdentifier, projectTask), HttpStatus.OK);
    }

}

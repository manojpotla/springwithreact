package com.mpotla.ppmtool.controllers;


import com.mpotla.ppmtool.domain.User;
import com.mpotla.ppmtool.services.ApiValidationService;
import com.mpotla.ppmtool.services.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApiValidationService apiValidationService;

    @PostMapping("/api/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody User user, BindingResult bindingResult){
        Map<String, String> errorMap = apiValidationService.validateApiRequest(bindingResult);

        if(errorMap != null){
            return new ResponseEntity<>(errorMap, HttpStatus.NOT_ACCEPTABLE);
        }
        User createdUser = userService.createUser(user);
        return new ResponseEntity(createdUser, HttpStatus.OK);
    }
}

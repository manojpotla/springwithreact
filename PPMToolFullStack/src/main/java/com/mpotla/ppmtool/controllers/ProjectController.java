package com.mpotla.ppmtool.controllers;

import com.mpotla.ppmtool.domain.Project;
import com.mpotla.ppmtool.services.ApiValidationService;
import com.mpotla.ppmtool.services.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api/project")
public class ProjectController {

    private ProjectService projectService;
    private ApiValidationService apiValidationService;

    public ProjectController(ProjectService projectServiceImpl, ApiValidationService apiValidationServiceImpl){
        this.projectService = projectServiceImpl;
        this.apiValidationService = apiValidationServiceImpl;
    }

    @PostMapping("")
    public ResponseEntity<?> createNewProject(@Valid @RequestBody Project project, BindingResult bindingResult){
        Map<String,String> errorMap = apiValidationService.validateApiRequest(bindingResult);
        if(errorMap != null)
            return new ResponseEntity<Map<String,String>>(errorMap, HttpStatus.BAD_REQUEST);

        projectService.saveOrUpdate(project);

        final ResponseEntity<Project> projectResponseEntity = new ResponseEntity<>(project, HttpStatus.CREATED);
        return projectResponseEntity;
    }

    @GetMapping("/{projectIdentifier}")
    public ResponseEntity<Project> retrieveProject(@PathVariable("projectIdentifier") String projectIdentifier){
        Project project = projectService.retrieveProject(projectIdentifier);
        return new ResponseEntity<>(project,HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<Iterable<Project>> findAllProjects(){
        return new ResponseEntity<>(projectService.findAllProjects(), HttpStatus.OK);
    }

    @DeleteMapping("/{projectIdentifier}")
    public ResponseEntity<String> deleteProject(@PathVariable("projectIdentifier") String projectIdentifier){
        return new ResponseEntity<String>(projectService.deleteProject(projectIdentifier), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<?> editProject(@Valid @RequestBody Project project, BindingResult bindingResult){
        Map<String, String> errorMap = apiValidationService.validateApiRequest(bindingResult);

        if(errorMap != null){
            return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
        }
        Project updatedProject = projectService.editProject(project);
        return new ResponseEntity<>(updatedProject, HttpStatus.OK);
    }
}

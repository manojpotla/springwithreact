package com.mpotla.ppmtool.controllers;

import com.mpotla.ppmtool.SecurityConstants;
import com.mpotla.ppmtool.dto.LoginRequest;
import com.mpotla.ppmtool.dto.LoginResponse;
import com.mpotla.ppmtool.exceptions.InvalidCredentialsException;
import com.mpotla.ppmtool.exceptions.UserNotFoundException;
import com.mpotla.ppmtool.security.JwtTokenProvider;
import com.mpotla.ppmtool.services.ApiValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    private ApiValidationService validationService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("/api/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult bindingResult) throws Exception {
        Authentication authentication;
        try {
            Map<String, String> apiErrors = validationService.validateApiRequest(bindingResult);
            if (apiErrors != null) {
                return new ResponseEntity<>(apiErrors, HttpStatus.BAD_REQUEST);
            }
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        } catch (UsernameNotFoundException | BadCredentialsException ex) {
            throw new InvalidCredentialsException("Invalid Username / password");
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwtToken = SecurityConstants.TOKEN_PREFIX + jwtTokenProvider.generateToken(authentication);

        return new ResponseEntity<>(new LoginResponse(jwtToken), HttpStatus.OK);
    }
}

package com.mpotla.ppmtool.security;

import com.mpotla.ppmtool.SecurityConstants;
import com.mpotla.ppmtool.domain.User;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class is used to- generateToken
 *                 - validateToken
 *                 - getUserId
 */

@Component
public class JwtTokenProvider {

    Logger jwtLogger = LoggerFactory.getLogger(JwtTokenProvider.class);

    public String generateToken(Authentication authentication){
        User user = (User)authentication.getPrincipal();
        Date now = new Date(System.currentTimeMillis());
        Date expiryDate = new Date(now.getTime() + SecurityConstants.EXPIRATION_TIME);
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", (Long.toString(user.getId())));
        claims.put("username", user.getUsername());
        claims.put("fullname", user.getFullname());

        String token = Jwts.builder().setSubject(Long.toString(user.getId()))
                             .setClaims(claims).setIssuedAt(now)
                             .setIssuedAt(now)
                             .setExpiration(expiryDate)
                             .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET_KEY)
                             .compact();
        jwtLogger.debug("Token is generated. Generated token is: " + Jwts.parser().setSigningKey(SecurityConstants.SECRET_KEY).parseClaimsJws(token));
        return token;
    }

    public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(SecurityConstants.SECRET_KEY).parseClaimsJws(token);
            jwtLogger.debug(Jwts.parser().setSigningKey(SecurityConstants.SECRET_KEY).parseClaimsJws(token).getBody().getExpiration().toString());
            return true;
        }catch (SignatureException | MalformedJwtException | ExpiredJwtException | UnsupportedJwtException | IllegalArgumentException ex){
            ex.getMessage();
        }
        return false;
    }

    public Long getUserIdFromJwt(String token){
        Claims claims = Jwts.parser().setSigningKey(SecurityConstants.SECRET_KEY).parseClaimsJws(token).getBody();
        String id = (String)claims.get("id");
        return Long.parseLong(id);
    }


}
